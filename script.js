const table = document.createElement("table");
const tr = document.createElement("tr");
const td = document.createElement("td");
const body = document.querySelector('body');

for (let i = 1; i <= 30; i++) {
  const cloneTr = tr.cloneNode(true);
  for (let x = 1; x <= 30; x++) {
    const cloneTd = td.cloneNode(true);
    cloneTd.classList.add('colored-white')
    cloneTr.append(cloneTd);
  }
  table.append(cloneTr);
}
document.body.prepend(table);

table.addEventListener('click', event => {
    event.target.classList.toggle('colored-red');
    event.target.classList.toggle('colored-white');
});

document.body.addEventListener('click', (event)=>{
    if (event.target === body) {
        table.classList.toggle('table');
    }
})